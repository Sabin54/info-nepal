package com.kozu.information.Hospitals;

public class HospitalDataModel {
    private String is_full;
    private String name;
    private String contact_person;
    private String contact_person_number;
    private String phone;
    private String notes;
    private String hospital_id;
    private String state;
    private String source;
    private String updated_at;

    public HospitalDataModel(String is_full, String name, String contact_person,
                             String contact_person_number, String phone, String notes,
                             String hospital_id,  String state, String source, String updated_at)
    {
        this.is_full = is_full;
        this.name = name;
        this.contact_person = contact_person;
        this.contact_person_number = contact_person_number;
        this.phone = phone;
        this.notes = notes;
        this.hospital_id = hospital_id;
        this.state = state;
        this.source = source;
        this.updated_at = updated_at;

    }

    public String getIs_full() {
        return is_full;
    }

    public void setIs_full(String is_full) {
        this.is_full = is_full;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getContact_person_number() {
        return contact_person_number;
    }

    public void setContact_person_number(String contact_person_number) {
        this.contact_person_number = contact_person_number;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}

package com.kozu.information.Hospitals;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kozu.information.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HospitalDataAdapter extends RecyclerView.Adapter<HospitalDataAdapter.HospitalViewHolder> implements Filterable {
    Context mCtx;
    List<HospitalDataModel> hospitalList;
    List<HospitalDataModel> FilteredList;
    boolean showContent = true;

    public HospitalDataAdapter(Context mCtx, List<HospitalDataModel> hospitalList) {
        this.mCtx = mCtx;
        this.hospitalList = hospitalList;
        this.FilteredList = hospitalList;
    }

    @Override
    public HospitalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.hospital_list_layout,parent,false);
        mCtx = parent.getContext();
        return new HospitalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HospitalViewHolder holder, int position) {
        final HospitalDataModel hospitalDataModel =hospitalList.get(position);


        holder.Numbering.setText(String.valueOf(position+1));
        holder.HospitalName.setText(hospitalDataModel.getName());
        holder.isFull.setText(hospitalDataModel.getIs_full());
        holder.personName.setText(hospitalDataModel.getContact_person());
        holder.pNumber.setText(hospitalDataModel.getContact_person_number());
        holder.hospitalNumber.setText(hospitalDataModel.getPhone());
        holder.hospital_id.setText(hospitalDataModel.getHospital_id());
        holder.state.setText(hospitalDataModel.getState());
        holder.Hsource.setText(hospitalDataModel.getSource());

        SimpleDateFormat formatDate = new SimpleDateFormat("dd MMM,yyyy");
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            Date inputDate = inputFormat.parse(hospitalDataModel.getUpdated_at());
            String postDateStr = formatDate.format(inputDate);
            holder.time.setText(postDateStr);
        } catch (ParseException e) {
            Log.d("TAG", "Error in Parsing date");
        }

        holder.show.setVisibility(View.GONE);
        holder.more.setText("Read more...");
        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showContent)
                {
                    holder.show.setVisibility(View.VISIBLE);
                    holder.more.setText("Hide");
                    showContent=false;
                }
                else
                {
                    holder.show.setVisibility(View.GONE);
                    showContent = true;
                    holder.more.setText("Read more...");
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return hospitalList.size();
    }

    public class HospitalViewHolder extends RecyclerView.ViewHolder{


        TextView HospitalName,isFull,personName, pNumber, hospitalNumber, hospital_id, state, Hsource, time,Numbering,more;
        LinearLayout show;
        public HospitalViewHolder(View itemView) {
            super(itemView);
            HospitalName = itemView.findViewById(R.id.HospitalName);
            isFull = itemView.findViewById(R.id.isFull);
            personName = itemView.findViewById(R.id.personName);
            pNumber = itemView.findViewById(R.id.pNumber);
            hospitalNumber = itemView.findViewById(R.id.hospitalNumber);
            hospital_id = itemView.findViewById(R.id.hospital_id);
            state = itemView.findViewById(R.id.state);
            Hsource = itemView.findViewById(R.id.Hsource);
            time = itemView.findViewById(R.id.time);
            Numbering = itemView.findViewById(R.id.Numbering);
            more = itemView.findViewById(R.id.more);

            show = itemView.findViewById(R.id.show);

        }

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    hospitalList = FilteredList;
                } else {
                    List<HospitalDataModel> filteredList1 = new ArrayList<>();
                    for (HospitalDataModel row : FilteredList) {
                        if ( row.getName().toLowerCase().contains(charSequence)||row.getState().toLowerCase().contains(charSequence)) {
                            filteredList1.add(row);
                        }
                    }

                    hospitalList = filteredList1;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = hospitalList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                hospitalList = (ArrayList<HospitalDataModel>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}

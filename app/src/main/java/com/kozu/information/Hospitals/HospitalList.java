package com.kozu.information.Hospitals;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.kozu.information.Constants;
import com.kozu.information.R;
import com.kozu.information.RequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HospitalList extends AppCompatActivity {

    List<HospitalDataModel> hospitalList;
    RecyclerView recyclerView;
    ProgressDialog pd;
    SearchView searchView;
    HospitalDataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view);

        hospitalList = new ArrayList<>();
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.show();

        recyclerView = findViewById(R.id.recycleView);

        LoadHospitals();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new HospitalDataAdapter(getApplicationContext(), hospitalList);
        recyclerView.setAdapter(adapter);


    }

    public void LoadHospitals()
    {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Constants.URL_HOSPITALS, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                pd.cancel();
                try {
                    JSONArray data = response.getJSONArray("data");
                    Log.d("TAG12", data.toString());
                    for (int i=0; i<data.length();i++)
                    {
                        JSONObject news = data.getJSONObject(i);
                        hospitalList.add(new HospitalDataModel(

                                news.getString("is_full"),
                                news.getString("name"),
                                news.getString("contact_person"),
                                news.getString("contact_person_number"),
                                news.getString("phone"),
                                news.getString("notes"),
                                news.getString("hospital_id"),
                                news.getString("state"),
                                news.getString("source"),
                                news.getString("updated_at")

                        ));

                        adapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TAG12", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
            }
        });

        // Adding request to request queue
        RequestHandler.getInstance(this).addToRequestQueue(jsonObjReq);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Hospital Name / State Number");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return true;
            }
        });

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id==R.id.action_search)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

}

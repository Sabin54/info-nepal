package com.kozu.information;

public class Constants {

    public static final String URL_COVID_NEPAL = "https://nepalcorona.info/api/v1/data/nepal";
    public static final String URL_COVID_WORLD = "https://data.nepalcorona.info/api/v1/world";
    public static final String URL_COVID_NEWS = "https://nepalcorona.info/api/v1/news";
    public static final String URL_HOSPITALS = "https://nepalcorona.info/api/v1/hospitals";
    public static final String URL_FAQ = " https://nepalcorona.info/api/v1/faqs";
    public static final String URL_DATA_COUNTRIES = " https://nepalcorona.info/api/v1/data/world";
    public static final String URL_COVID_HOSTORY = " https://data.nepalcorona.info/api/v1/covid/timeline";

}

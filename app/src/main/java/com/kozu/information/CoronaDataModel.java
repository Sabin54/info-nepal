package com.kozu.information;

public class CoronaDataModel {
    private String tested_positive;
    private String tested_negative;
    private String tested_total;
    private String in_isolation;
    private String quarantined;
    private String tested_rdt;
    private String recovered;
    private String deaths;
    private String source;
    private String updated_at;

    public CoronaDataModel(String tested_positive, String tested_negative, String tested_total,
                           String in_isolation, String quarantined, String recovered,  String deaths, String source, String updated_at) {

        this.tested_positive = tested_positive;
        this.tested_negative = tested_negative;
        this.tested_total = tested_total;
        this.in_isolation = in_isolation;
        this.quarantined = quarantined;
        this.recovered = recovered;
        this.deaths = deaths;
        this.source = source;
        this.updated_at = updated_at;
    }


}

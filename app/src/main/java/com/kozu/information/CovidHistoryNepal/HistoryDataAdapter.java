package com.kozu.information.CovidHistoryNepal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kozu.information.R;

import java.util.ArrayList;
import java.util.List;

public class HistoryDataAdapter extends RecyclerView.Adapter<HistoryDataAdapter.HistoryViewHolder> implements Filterable {
    Context mCtx;
    List<HistorydataModel> dataList;
    List<HistorydataModel> FilteredList;

    public HistoryDataAdapter(Context mCtx, List<HistorydataModel> dataList) {
        this.mCtx = mCtx;
        this.dataList = dataList;
        this.FilteredList = dataList;
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.covid_history_nepal,parent,false);
        mCtx = parent.getContext();
        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        final HistorydataModel historydataModel =dataList.get(position);


        holder.NrecoveredNo.setText(historydataModel.getNewRecoveries());
        holder.Ncase.setText(historydataModel.getNewCases());
        holder.Ndeath.setText(historydataModel.getNewDeaths());
        holder.NdateN.setText(historydataModel.getDate());

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder{


        TextView NrecoveredNo, Ncase, Ndeath, NdateN;
        public HistoryViewHolder(View itemView) {
            super(itemView);

            NrecoveredNo = itemView.findViewById(R.id.NrecoveredNo);
            Ncase = itemView.findViewById(R.id.Ncase);
            Ndeath = itemView.findViewById(R.id.Ndeath);
            NdateN = itemView.findViewById(R.id.NdateN);

        }

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataList = FilteredList;
                } else {
                    List<HistorydataModel> filteredList1 = new ArrayList<>();
                    for (HistorydataModel row : FilteredList) {
                        if ( row.getDate().toLowerCase().contains(charSequence)) {
                            filteredList1.add(row);
                        }
                    }

                    dataList = filteredList1;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                dataList = (ArrayList<HistorydataModel>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}

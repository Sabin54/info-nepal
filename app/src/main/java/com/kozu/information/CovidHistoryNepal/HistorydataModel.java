package com.kozu.information.CovidHistoryNepal;

public class HistorydataModel {
    private String newCases;
    private String newRecoveries;
    private String newDeaths;
    private String date;


    public HistorydataModel(String newRecoveries, String newCases,   String newDeaths, String date)
    {

        this.newRecoveries =newRecoveries;
        this.newCases =newCases;
        this.newDeaths =newDeaths;
        this.date =date;
    }

    public String getNewCases() {
        return newCases;
    }

    public void setNewCases(String newCases) {
        this.newCases = newCases;
    }

    public String getNewRecoveries() {
        return newRecoveries;
    }

    public void setNewRecoveries(String newRecoveries) {
        this.newRecoveries = newRecoveries;
    }

    public String getNewDeaths() {
        return newDeaths;
    }

    public void setNewDeaths(String newDeaths) {
        this.newDeaths = newDeaths;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

package com.kozu.information;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.kozu.information.CovidHistoryNepal.CovidHistoryNepal;
import com.kozu.information.FQA.FAQ;
import com.kozu.information.Hospitals.HospitalList;
import com.kozu.information.News.News;
import com.kozu.information.OtherCountriesData.OCD;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.kozu.information.R;

public class MoreOptions  extends AppCompatActivity {
    BottomNavigationView bottom_navigation;
    CardView Hospitals,news,Enumbers,otherCountries,nepalHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_options);

        Hospitals = findViewById(R.id.hospital);
        news = findViewById(R.id.news);
        Enumbers = findViewById(R.id.Enumbers);
        otherCountries = findViewById(R.id.otherCountries);
        nepalHistory = findViewById(R.id.nepalHistory);

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), News.class));
            }
        });
        Hospitals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), HospitalList.class));
            }
        });

        Enumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), FAQ.class));
            }
        });

        otherCountries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), OCD.class));
            }
        });

        nepalHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CovidHistoryNepal.class));
            }
        });


        bottom_navigation = findViewById(R.id.bottom_navigation);
        bottom_navigation.setSelectedItemId(R.id.more);
        bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nepal:
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                    case R.id.world:
                        startActivity(new Intent(getApplicationContext(),WorldData.class));
                        overridePendingTransition(0,0);
                        return true;

                    case R.id.more:
                        return true;
                }

                return false;
            }
        });

    }
}
package com.kozu.information.FQA;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kozu.information.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FAQDataAdapter extends RecyclerView.Adapter<FAQDataAdapter.FaqViewHolder> {
    Context mCtx;
    List<FAQDataModel> FQAList;


    public FAQDataAdapter(Context mCtx, List<FAQDataModel> FQAList) {
        this.mCtx = mCtx;
        this.FQAList = FQAList;
    }

    @Override
    public FaqViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.fqa,parent,false);
        mCtx = parent.getContext();
        return new FaqViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FaqViewHolder holder, int position) {
        final FAQDataModel faqDataModel =FQAList.get(position);


        holder.Numbering.setText(String.valueOf(position+1));
        holder.question.setText(faqDataModel.getQuestion());
        holder.answer.setText(faqDataModel.getAnswer());
        holder.questionNp.setText(faqDataModel.getQuestion_np());
        holder.answerNp.setText(faqDataModel.getAnswer_np());

        SimpleDateFormat formatDate = new SimpleDateFormat("dd MMM,yyyy");
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            Date inputDate = inputFormat.parse(faqDataModel.getDate());
            String postDateStr = formatDate.format(inputDate);
            holder.date.setText(postDateStr);
        } catch (ParseException e) {
            Log.d("TAG", "Error in Parsing date");
        }

        holder.Nepali.setVisibility(View.GONE);
        holder.Eng.setBackgroundColor(Color.parseColor("#FFFFD1"));

        holder.Nep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.Nepali.setVisibility(View.VISIBLE);
                holder.English.setVisibility(View.GONE);
                holder.Nep.setBackgroundColor(Color.parseColor("#FFFFD1"));
                holder.Eng.setBackgroundColor(Color.parseColor("#CEFCC4"));


            }
        });

        holder.Eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.Nepali.setVisibility(View.GONE);
                holder.English.setVisibility(View.VISIBLE);
                holder.Eng.setBackgroundColor(Color.parseColor("#FFFFD1"));
                holder.Nep.setBackgroundColor(Color.parseColor("#CEFCC4"));
            }
        });




    }

    @Override
    public int getItemCount() {
        return FQAList.size();
    }

    public class FaqViewHolder extends RecyclerView.ViewHolder{


        TextView question,answer,questionNp, answerNp, date, Numbering;
        TextView Eng,Nep;
        LinearLayout English,Nepali;

        public FaqViewHolder(View itemView) {
            super(itemView);
            Numbering = itemView.findViewById(R.id.Numbering1);
            question = itemView.findViewById(R.id.question);
            answer = itemView.findViewById(R.id.answer);
            questionNp = itemView.findViewById(R.id.questionNp);
            answerNp = itemView.findViewById(R.id.answerNp);
            date = itemView.findViewById(R.id.date);

            Eng = itemView.findViewById(R.id.Eng);
            Nep = itemView.findViewById(R.id.Nep);
            English = itemView.findViewById(R.id.English);
            Nepali = itemView.findViewById(R.id.Nepali);


        }

    }
}

package com.kozu.information.FQA;

public class FAQDataModel {
    private String question;
    private String answer;
    private String question_np;
    private String answer_np;
    private String date;

    public FAQDataModel(String question, String answer, String question_np, String answer_np, String date)
    {
        this.answer =answer;
        this.question =question;
        this.question_np =question_np;
        this.answer_np =answer_np;
        this.date =date;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion_np() {
        return question_np;
    }

    public void setQuestion_np(String question_np) {
        this.question_np = question_np;
    }

    public String getAnswer_np() {
        return answer_np;
    }

    public void setAnswer_np(String answer_np) {
        this.answer_np = answer_np;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

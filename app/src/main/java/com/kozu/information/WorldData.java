package com.kozu.information;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.kozu.information.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WorldData extends AppCompatActivity {
    BottomNavigationView bottom_navigation;
    TextView population, tRecovered, tDeath, tNCase, tTest, Tcases, TdeathNo, TrecoveredNo, tActivecase, Tdate;
    Button refresh;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.world_data);

        //population = findViewById(R.id.population);
        tRecovered = findViewById(R.id.tRecovered);
        tDeath = findViewById(R.id.tDeath);
        tNCase = findViewById(R.id.tNCase);
        tTest = findViewById(R.id.tTest);
        Tcases = findViewById(R.id.Tcases);
        TdeathNo = findViewById(R.id.TdeathNo);
        TrecoveredNo = findViewById(R.id.TrecoveredNo);
        tActivecase = findViewById(R.id.tActivecase);
        Tdate = findViewById(R.id.Tdate);

        refresh = findViewById(R.id.refresh);

        pd = new ProgressDialog(this);
        pd.setMessage("loading...");
        pd.show();

        makeJsonObjectRequest();

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();
                makeJsonObjectRequest();
            }
        });

        bottom_navigation = findViewById(R.id.bottom_navigation);
        bottom_navigation.setSelectedItemId(R.id.world);
        bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nepal:
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                    case R.id.world:
                        return true;

                    case R.id.more:
                        startActivity(new Intent(getApplicationContext(),MoreOptions.class));
                        overridePendingTransition(0,0);
                        return true;
                }

                return false;
            }
        });
    }

    private void makeJsonObjectRequest() {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Constants.URL_COVID_WORLD, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("WORLD", response.toString());
                pd.cancel();
                try {
                    // Parsing json object response
                    // response will be a json object
                    String TotalCases = response.getString("cases");
                    String TcaseToday = response.getString("todayCases");
                    String TrecoveredToday = response.getString("todayRecovered");
                    String TactiveCase = response.getString("active");
                    String Ttests = response.getString("tests");
                    String Tpopulation = response.getString("population");
                    String TdeathToday = response.getString("todayDeaths");
                    String TNumberRecovered = response.getString("recovered");
                    String TNumberdeath = response.getString("deaths");
                    String Dupdate = response.getString("updated");
                    String critical = response.getString("critical");

                    //population.setText(Tpopulation);
                    tRecovered.setText(TrecoveredToday);
                    TrecoveredNo.setText(TNumberRecovered);
                    tDeath.setText(TdeathToday);
                    Tcases.setText(TotalCases);
                    tNCase.setText(TcaseToday);
                    tActivecase.setText(TactiveCase);
                    tTest.setText(Ttests);
                    TdeathNo.setText(TNumberdeath);

                    long time = Long.parseLong(Dupdate);
                    Date date = new Date(time);
                    SimpleDateFormat formatDate = new SimpleDateFormat("dd MMM,yyyy");
                    String formated = formatDate.format(date);

                    Tdate.setText(formated);




                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TAG", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
            }
        });

        // Adding request to request queue
        RequestHandler.getInstance(this).addToRequestQueue(jsonObjReq);
    }
}

package com.kozu.information.News;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.kozu.information.Constants;
import com.kozu.information.R;
import com.kozu.information.RequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class News extends AppCompatActivity {

    List<NewsDataModel> newsList;
    RecyclerView recyclerView;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view);

        newsList = new ArrayList<>();
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.show();

        recyclerView = findViewById(R.id.recycleView);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        LoadNews();
    }

    public void LoadNews()
    {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Constants.URL_COVID_NEWS, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                pd.cancel();
                try {
                    JSONArray data = response.getJSONArray("data");
                    Log.d("TAG12", data.toString());
                    for (int i=0; i<data.length();i++)
                    {
                        JSONObject news = data.getJSONObject(i);
                        newsList.add(new NewsDataModel(

                                news.getString("title"),
                                news.getString("image_url"),
                                news.getString("summary"),
                                news.getString("source"),
                                news.getString("url"),
                                news.getString("updated_at")

                        ));
                        NewsAdapter adapter = new NewsAdapter(getApplicationContext(), newsList);
                        recyclerView.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TAG12", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
            }
        });

        // Adding request to request queue
        RequestHandler.getInstance(this).addToRequestQueue(jsonObjReq);
    }
}

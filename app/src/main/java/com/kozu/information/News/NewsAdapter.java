package com.kozu.information.News;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kozu.information.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder>{
    Context mCtx;
    List<NewsDataModel> newsList;

    public NewsAdapter(Context mCtx, List<NewsDataModel> newsList) {
        this.mCtx = mCtx;
        this.newsList = newsList;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.news_layout,parent,false);
        mCtx = parent.getContext();
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        final NewsDataModel newsDataModel =newsList.get(position);

        holder.newsTitle.setText(newsDataModel.getTitle());
        Glide.with(mCtx)
                .load(newsDataModel.getImage_url())
                .into(holder.imageV);
        //Log.d("TAGCourse1", "onBindViewHolder: "+ courseDataModel.getImage());

        holder.summary.setText(newsDataModel.getSummary());
        //holder.source.setText(newsDataModel.getSource());

        SimpleDateFormat formatDate = new SimpleDateFormat("dd MMM,yyyy");
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            Date inputDate = inputFormat.parse(newsDataModel.getTime());
            String postDateStr = formatDate.format(inputDate);
            holder.time.setText(postDateStr);
        } catch (ParseException e) {
            Log.d("TAG", "Error in Parsing date");
        }





    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder{

        ImageView imageV;
        TextView newsTitle,summary,link, source, time;
        public NewsViewHolder(View itemView) {
            super(itemView);
                newsTitle = itemView.findViewById(R.id.newsTitle);
                summary = itemView.findViewById(R.id.summary);
                time = itemView.findViewById(R.id.time);
                imageV = itemView.findViewById(R.id.imageV);

        }

    }
}

package com.kozu.information.News;

public class NewsDataModel {
    private String source;
    private String title;
    private String image_url;
    private String summary;
    private String link;
    private String time;

    public NewsDataModel( String title, String image_url,
                         String summary, String link,String source, String time)
    {
        this.title = title;
        this.image_url=image_url;
        this.summary = summary;
        this.link=link;
        this.source = source;
        this.time = time;

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

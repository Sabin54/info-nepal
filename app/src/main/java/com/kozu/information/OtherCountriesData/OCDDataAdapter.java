package com.kozu.information.OtherCountriesData;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kozu.information.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OCDDataAdapter extends RecyclerView.Adapter<OCDDataAdapter.OCDViewHolder> implements Filterable {
    Context mCtx;
    List<OCDDataModel> CountryList;
    List<OCDDataModel> FilteredList;

    public OCDDataAdapter(Context mCtx, List<OCDDataModel> CountryList) {
        this.mCtx = mCtx;
        this.CountryList = CountryList;
        this.FilteredList = CountryList;
    }

    @Override
    public OCDViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.other_countries_data,parent,false);
        mCtx = parent.getContext();
        return new OCDViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OCDViewHolder holder, int position) {
        final OCDDataModel ocdDataModel =CountryList.get(position);


        holder.countryName.setText(ocdDataModel.getCountry());
        holder.tDeath.setText(ocdDataModel.getNewDeaths());
        holder.tNCase.setText(ocdDataModel.getNewCases());
        holder.tTest.setText(ocdDataModel.getTests());
        holder.Tcases.setText(ocdDataModel.getTotalCases());
        holder.TdeathNo.setText(ocdDataModel.getTotalDeaths());
        holder.TrecoveredNo.setText(ocdDataModel.getTotalRecovered());
        holder.tActivecase.setText(ocdDataModel.getActiveCases());

        SimpleDateFormat formatDate = new SimpleDateFormat("dd MMM,yyyy");
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            Date inputDate = inputFormat.parse(ocdDataModel.getUpdated());
            String postDateStr = formatDate.format(inputDate);
            holder.Tdate.setText(postDateStr);
        } catch (ParseException e) {
            Log.d("TAG", "Error in Parsing date");
        }

    }

    @Override
    public int getItemCount() {
        return CountryList.size();
    }

    public class OCDViewHolder extends RecyclerView.ViewHolder{


        TextView  countryName, tDeath, tNCase, tTest, Tcases, TdeathNo, TrecoveredNo, tActivecase, Tdate;
        public OCDViewHolder(View itemView) {
            super(itemView);

            countryName = itemView.findViewById(R.id.countryname);
            tDeath = itemView.findViewById(R.id.tDeath);
            tNCase = itemView.findViewById(R.id.tNCase);
            tTest = itemView.findViewById(R.id.tTest);
            Tcases = itemView.findViewById(R.id.Tcases);
            TdeathNo = itemView.findViewById(R.id.TdeathNo);
            TrecoveredNo = itemView.findViewById(R.id.TrecoveredNo);
            tActivecase = itemView.findViewById(R.id.tActivecase);
            Tdate = itemView.findViewById(R.id.Tdate);

        }

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    CountryList = FilteredList;
                } else {
                    List<OCDDataModel> filteredList1 = new ArrayList<>();
                    for (OCDDataModel row : FilteredList) {
                        if ( row.getCountry().toLowerCase().contains(charSequence)) {
                            filteredList1.add(row);
                        }
                    }

                    CountryList = filteredList1;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = CountryList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                CountryList = (ArrayList<OCDDataModel>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}

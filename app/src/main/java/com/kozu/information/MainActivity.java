package com.kozu.information;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.kozu.information.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    TextView date, totalTest, isolation, quarantined, testedRDT, source, deathNo, recoveredNo, positiveNo, negativeNo;
    Button refresh;
    ProgressDialog pd;
    String sources;

    BottomNavigationView bottom_navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pd = new ProgressDialog(this);
        pd.setMessage("loading...");
        pd.show();

        date = findViewById(R.id.date);
        totalTest = findViewById(R.id.totalTest);
        isolation = findViewById(R.id.isolation);
        quarantined = findViewById(R.id.quarantined);
        testedRDT = findViewById(R.id.testedRDT);
        source = findViewById(R.id.source);
        deathNo = findViewById(R.id.deathNo);
        recoveredNo = findViewById(R.id.recoveredNo);
        positiveNo = findViewById(R.id.positiveNo);
        negativeNo = findViewById(R.id.negativeNo);

        refresh = findViewById(R.id.refresh);
        bottom_navigation = findViewById(R.id.bottom_navigation);
        bottom_navigation.setSelectedItemId(R.id.nepal);
        bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nepal:
                        return true;

                    case R.id.world:
                        startActivity(new Intent(getApplicationContext(),WorldData.class));
                        overridePendingTransition(0,0);
                        return true;

                    case R.id.more:
                        startActivity(new Intent(getApplicationContext(),MoreOptions.class));
                        overridePendingTransition(0,0);
                        return true;
                }

                return false;
            }
        });

        makeJsonObjectRequest();

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();
                makeJsonObjectRequest();
            }
        });

        source.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sources));
                startActivity(browserIntent);
            }
        });


    }

    private void makeJsonObjectRequest() {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Constants.URL_COVID_NEPAL, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("TAG", response.toString());
                pd.cancel();

                try {
                    // Parsing json object response
                    // response will be a json object
                    String Tpositive = response.getString("tested_positive");
                    sources = response.getString("source");
                    String Tnegative = response.getString("tested_negative");
                    String Ttest = response.getString("tested_total");
                    String Tisolation = response.getString("in_isolation");
                    String Tquarantined = response.getString("quarantined");
                    String Trdt = response.getString("tested_rdt");
                    String Trecovered = response.getString("recovered");
                    String Tdeath = response.getString("deaths");
                    String Dupdate = response.getString("updated_at");

                    totalTest.setText(Ttest);
                    deathNo.setText(Tdeath);
                    recoveredNo.setText(Trecovered);
                    negativeNo.setText(Tnegative);
                    positiveNo.setText(Tpositive);
                    isolation.setText(Tisolation);
                    quarantined.setText(Tquarantined);
                    testedRDT.setText(Trdt);
                    //Date Format
                    SimpleDateFormat formatDate = new SimpleDateFormat("dd MMM,yyyy");
                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

                    try {
                        Date inputDate = inputFormat.parse(Dupdate);
                        String postDateStr = formatDate.format(inputDate);
                        date.setText(postDateStr);
                    } catch (ParseException e) {
                        Log.d("TAG", "Error in Parsing date");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TAG", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
            }
        });

        // Adding request to request queue
        RequestHandler.getInstance(this).addToRequestQueue(jsonObjReq);
    }

}
